"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.computedMappings = void 0;
const language_core_1 = require("@volar/language-core");
const computeds_1 = require("computeds");
const utils_1 = require("../generators/utils");
function computedMappings(snapshot, sfc) {
    return (0, computeds_1.computed)(() => {
        const str = [[snapshot().getText(0, snapshot().getLength()), undefined, 0, (0, utils_1.enableAllFeatures)({})]];
        for (const block of [
            sfc.script,
            sfc.scriptSetup,
            sfc.template,
            ...sfc.styles,
            ...sfc.customBlocks,
        ]) {
            if (block) {
                (0, language_core_1.replaceSourceRange)(str, undefined, block.startTagEnd, block.endTagStart, '\n\n');
            }
        }
        const mappings = str
            .filter(s => typeof s !== 'string')
            .map(m => {
            const text = m[0];
            const start = m[2];
            return {
                sourceOffsets: [start],
                generatedOffsets: [start],
                lengths: [text.length],
                data: m[3],
            };
        });
        // fix folding range end position failed to mapping
        for (const block of [
            sfc.script,
            sfc.scriptSetup,
            sfc.template,
            ...sfc.styles,
            ...sfc.customBlocks,
        ]) {
            const offsets = [];
            if (block) {
                let content = block.content;
                if (content.endsWith('\r\n')) {
                    content = content.slice(0, -2);
                }
                else if (content.endsWith('\n')) {
                    content = content.slice(0, -1);
                }
                const offset = content.lastIndexOf('\n') + 1;
                offsets.push(block.startTagEnd + offset);
            }
            if (offsets.length) {
                mappings.push({
                    sourceOffsets: offsets,
                    generatedOffsets: offsets,
                    lengths: offsets.map(() => 0),
                    data: (0, utils_1.disableAllFeatures)({ structure: true }),
                });
            }
        }
        return mappings;
    });
}
exports.computedMappings = computedMappings;
//# sourceMappingURL=computedMappings.js.map