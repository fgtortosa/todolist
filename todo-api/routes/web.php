<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Models\TaskModel;

$router->get('/', function () use ($router) {
    return "hello world";
});


$router->get('/tasks', function () use ($router) {
    $data = \App\Models\TaskModel::all();
    return response($data);
});
