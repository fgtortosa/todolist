# **todo** Project

Proyecto para gestionar una lista de tareas diseañado para prácticar la tecnología Docker en un escenario real.

[Ejercicios](https://gitlab.com/curso_docker_2401/docs/-/blob/main/Ejercicios/sesion3/main.md?ref_type=heads)

## Soluciones a los ejercicios

### Creamos las redes

```bash
docker network create todo-frontend
docker network create todo-backend 
```

### todo-front

Modificamos el fichero `vite.config.ts` para usar el puerto 8080:

```js
import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    port: 8080
  }
})
```

Instalamos dependencias y arrancamos el servidor:

```bash
docker run -it --rm -v $(pwd)/todo-front:/app todo-builder "npm install"
docker run -d \
    --name todo-front \
    -p 8080:8080 \
    -v $(pwd)/todo-front:/app \
    --network=todo-frontend \
    todo-builder \
    "npm run dev"
```
> Comandos útiles:
>
> `docker logs -f todo-front`
>
> `docker exec -it todo-front bash`

### todo-back

Instalamos dependencias y arrancamos el servidor:

```bash
docker run -it --rm -v $(pwd)/todo-api:/app todo-builder "composer install"
docker run -d  \
    --name todo-api \
    -p 8081:8081 \
    -v $(pwd)/todo-api:/app \
    --network=todo-frontend \
    --network=todo-backend \
    todo-builder \
    "php -S 0.0.0.0:8081 -t public"
```

> Comandos útiles:
>
> `docker logs -f todo-api`
>
> `docker exec -it todo-api bash`

### todo-mariadb

Creamos un volumen:

```bash
docker volume create todo-mariadb
```

```bash
docker run -d \
    --name todo-mariadb \
    -v todo-mariadb:/var/lib/mysql:Z \
    --env MARIADB_ROOT_PASSWORD=password \
    --network=todo-backend \
     mariadb:latest 
```



